# nagios-puppet-manifest

Ngios Puppet Manifest Based on Debian source [instalation][nagios]

### Contents

- [Introduction](#introduction)
- [Using the manifest](#using-the-manifest)
  - [On the command line](#on-the-command-line)
- [Testing](#testing)
- [Contributing](#contributing)
- [License](#license)

## Introduction

## Image Versions
[Apache][apache] | `2.4.25`
[PHP][php] | `7.0.33`
[Nagios][nagios] | `4.4.3`
[Nagios-Plugins][nagios-plugins] | `2.2.1`


## Using the manifest
### On the command line
Copy and rename nagios.pp to site.pp on Puppet master on manifests path(Ex.: /puppet/code/environments/production/manifests/site.pp)

On client execute:
```bash
puppet agent -t
```
## Testing
Point your web browser to the client ip address, fo example:

http://10.25.5.143/nagios

## Contributing
If you wish to submit a bug fix or feature, you can create a pull request and it will be merged pending a code review.

1. Clone/fork it
2. Create your feature branch (git checkout -b my-new-feature)
3. Commit your changes (git commit -am 'Add some feature')
4. Push to the branch (git push origin my-new-feature)
5. Create a new Pull Request


## License
nagios-puppet-manifest is licensed under the [GNU General Public License v3.0][info-license].

[apache]: http://www.apache.org/
[nagios]: https://support.nagios.com/kb/article/nagios-core-installing-nagios-core-from-source-96.html#Debian
[nagios-plugins]: https://github.com/nagios-plugins/nagios-plugins/
[php]: http://php.net/

[info-license]: LICENSE

[nagios-puppet-manifest]: https://gitlab.com/cedon/nagios-puppet-manifest/
